const AWS = require('aws-sdk');
const fs = require('fs');

async function main({bucket, key, destination}) {

    if (!fs.existsSync(destination)){
        fs.mkdirSync(destination);
    }

    const s3 = new AWS.S3();
    const path = `${destination}/${key}`;
    const file = fs.createWriteStream(path, {
        flags: 'w+'
    });

    console.log(`> downloading from ${bucket}/${key}`);

    s3
        .getObject({
            Bucket: bucket,
            Key: key
        })
        .createReadStream()
        .on('error', (e) => {
            console.error('> error: ', e);

            // remove the file when error
            fs.unlinkSync(path);
        })
        .on('end', () => {
            console.log(`> download completed, saved to ${path}`);
        })
        .pipe(file);
}

main({
    bucket: process.env.S3_BUCKET,
    key: process.env.S3_OBJECT_KEY,
    destination: process.env.DST_DIRECTORY
});