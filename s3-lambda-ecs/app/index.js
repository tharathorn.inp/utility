'use strict';

const AWS = require('aws-sdk');
const ecs = new AWS.ECS();

module.exports.createHandler = function ({ecsCluster, ecsTask, ecsTaskName}) {
    return async function (event) {
        console.log('event received', JSON.stringify(event, null, 2));

        const s3Event = event.Records[0];
        const srcBucket = s3Event.s3.bucket.name;
        const srcKey = decodeURIComponent(s3Event.s3.object.key.replace(/\+/g, " "));

        try {
            const result = await ecs.runTask({
                cluster: ecsCluster,
                taskDefinition: ecsTask,
                overrides: {
                    containerOverrides: [
                        {
                            name: ecsTaskName,
                            environment: [
                                {
                                    name: 'S3_BUCKET',
                                    value: srcBucket
                                },
                                {
                                    name: 'S3_OBJECT_KEY',
                                    value: srcKey
                                }
                            ]
                        }
                    ]
                }
            }).promise();

            console.log('task running successfully', JSON.stringify(result, null, 2));

            return {message: 'Success'};

        } catch (e) {
            console.error('error running task', e);

            return {message: 'Failure'};
        }
    }
};