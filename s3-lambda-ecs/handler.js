'use strict';

const createHandler = require('./app').createHandler;

module.exports.runTaskAlpha = createHandler({
  ecsCluster: '',
  ecsTask: '',
  ecsTaskName: ''
});

module.exports.runTaskStaging = createHandler({
  ecsCluster: '',
  ecsTask: '',
  ecsTaskName: ''
});

module.exports.runTaskProduction = createHandler({
  ecsCluster: '',
  ecsTask: '',
  ecsTaskName: ''
});